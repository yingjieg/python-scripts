#!/usr/bin/env python
#coding:utf-8

import sys
import time
import subprocess



class Shell:
    def __init__(self, time=30):
        self.time = time;

    def runCmd(self, command):
        proc = subprocess.Popen(command, shell=True);
        status = proc.wait();
        return status;


# this function can get the output from screen.
    def runCmd2(self,command):
        proc = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
        return proc.stdout.readlines();

    def delay(self, t):
        self.time = t;
        while self.time > 0:
            time.sleep(0.1);
            sys.stderr.write('*');
            self.time = self.time - 1;
        print '\n';

#Added by EDUOWAN
#used to confirm an operation to continue.
    def operation_confirm(self,prompt = "Continue?[yes]"):
        if raw_input(prompt) in ('yes','ye','y',''):
            return True;
        else:
            return False;

        

if __name__ == "__main__":
    p = Shell();

    result = p.runCmd("ls -al");
    print result;
