#!/usr/bin/env python
#coding:utf-8

import os
import sys
import linecache
import shell.Shell as Shell

#convert hex repr to string
def toStr(s):
    return s and chr(atoi(s[:2], base=16)) + toStr(s[2:]) or '';


class Dump:
    def __init__(self, elfPath, dumpPath):
        self.elfPath = elfPath;
        self.dumpPath = dumpPath;
        self.shell = Shell.Shell();


    def print_msg(self):
        pass;

    def getModuleName(self):
# you should handle two condition of the path
# 1.   PEDRO7R.elf
# 2.   /home/eyieguo/PEDRO7R.elf
        if self.elfPath.find('/') != -1:
            leftIndex = self.elfPath.rindex('/');
        else:
            leftIndex = -1;
            rightIndex = self.elfPath.index('.');

        return self.elfPath[leftIndex+1:rightIndex];

    def getAsa(self):
        pass;

    def getTextAddress(self):
        contents = [];
        output = self.shell.runCmd2("powerpc-eabi-readelf -S " + self.elfPath);
        for line in output:
            if line.find(".text") != -1:
                contents = line.split();
                break;
        return contents[4];  # not a good way , hard code

        
    def addr2line(self, FuncAddrs):
        for i in FuncAddrs:
            self.shell.runCmd("powerpc-eabi-addr2line " + i + " -e" + self.elfPath + " -f");


# get the offset address from dump
# return list
    def getFuncAddr(self):
        funcAddrs = [];
        codeAddr = self.addrRange();
        addresses = self.findStack();
        for i in addresses:
            if int(i, 16) >= int(codeAddr[0], 16) and int(i, 16) <= int(codeAddr[1], 16):
                #print "0x"+i;
                offset = int(i, 16) - int(codeAddr[0], 16);
                print 'offset addr:',;
                print hex(offset),;
                start_addr = int(self.getTextAddress(), 16) - 64;
                temp = start_addr + offset;
                print "    real addr:",;
                print hex(temp);
                funcAddrs.append(hex(temp));

        return funcAddrs;

# analyze the dump file and restore the MAIN STACK address in list
# return list
    def findStack(self):
        fd = open(self.dumpPath);
        stack = [];
        flag = 0;
        for line in fd.readlines():
            if line.find("MAIN STACK FOR PROCESS") != -1: #not a good way 
                flag = 1;
            if flag == 1:
                if line == '\r\n':
                    flag = 0;
                    break;
                stack.append(line);
        address = [];
        for s in stack:
           if s.find(':') != -1:
               temp = s.split(' ');
               for t in temp:
                   if len(t) == 8:
                       address.append(t);
                   if len(t) == 10:
                       address.append(t[:-2]);
        fd.close();

        return address;


# get the range of  code address and data address
# return list
# list[0]-list[1] code address.  list[2]-list[3] data address
    def addrRange(self):
        addressRange = [];
        fd = open(self.dumpPath);
        lineNo = 0;
        for line in fd.readlines():
            lineNo = lineNo + 1;
            if line.find("Program:") != -1 and line.find(self.getModuleName()) != -1:
                break;

        fd.close();
        line = linecache.getline(self.dumpPath, lineNo + 1);
        temp = line.split(' ');
        for i in range(0, len(temp)):
            if temp[i].find('0x') != -1:
                addressRange.append(temp[i])

        print "code address " + addressRange[0] + " - " + addressRange[1];
        return addressRange;


    def analyzeLog(self):
        funcList = [];
        addressList = [];
        flag = 0;
        funcName = '';
        self.shell.runCmd("powerpc-eabi-objdump -d " + self.elfPath + " > temp.s")
        fd = open("temp.s", 'r');
        contents = fd.readlines();
        fd.close();
        lineNo = 0;
        temp = self.getFuncAddr();

        if len(temp) == 0:
            print "\n============================      Message     ======================================="
            print "Can't figure out the Function address in dump file\ncheck your elf file and dump file";
            print "=====================================================================================\n"
            return; 
# should os exit here.
        
        for line in contents:
            lineNo = lineNo + 1;

            if line.find(">:") != -1:
                funcName = line;
                flag = 1;
                continue;

            for t in temp:
                if line.find(t[2:]+':') != -1:
                    print funcName;
                    flag = 0;
                    funcName = '';
                    break;



if __name__ == "__main__":
    if len(sys.argv) == 3:
        print "elf file:",sys.argv[1],"log file:",sys.argv[2];
        choose=raw_input( "location above right? [yes|no] (ENTER for yes) >");
        if choose in ('y','ye','yes',''):
            a = Dump(sys.argv[1],sys.argv[2]);
        if choose in ('n','no'):
            elf=raw_input("please input elf file location:")
            log=raw_input("please input log file location:")
            a = Dump(elf,log);
    #    a.findStack();
    #    a.getModuleName();
    #    a.addrRange();
    #    a.getFuncAddr();
    #    a.getTextAddress();
    #    a.addr2line();
        a.analyzeLog();
        print "-----------------------------------------\n"
        a.addr2line(a.getFuncAddr());
    else:
        print "Usage: analyzeDump(/path/to/file.elf, /path/to/file.log)"




