#!/usr/bin/env python
#coding:utf-8

import shell.Shell as shell
import os
import sys
import commands



ct_path = "/opt/rational/clearcase/bin/cleartool";

class Vob:
    def __init__(self):
        self.sd = shell.Shell();


    def get_label(self, _keyword1, _keyword2, _vob_path):
        vob_path = _vob_path;
        keyword1 = _keyword1;
        keyword2 = _keyword2;

# here we can use dynamic keyword to grep the label.  hardcode is not a good way.
        cmd = ct_path+" lstype -nos -unsorted -kind lbtype -invob "+vob_path+" | grep "+keyword1+" | grep "+keyword2;
        return self.sd.runCmd2(cmd);

# generate config spec for pedro
# some hardcode, not good way, we can improve it.
    def generate_cs(self, template_path, cs_path, label_name, branch_name):
        cs = self.parse_cs(self.read_file(template_path), label_name, branch_name);
        try:
            f = open(cs_path, 'w');
            for line in cs:
                f.write(line);
            f.close();
        except:
            print "can not find the config spec"


    def read_file(self, file_path):
        try:
            fd = open(file_path,'r');
            contents = fd.readlines();
            return contents;
        except:
            print "can not find the template config spec, please check";
            sys.exit(1);

# contents: type is list.  store the config_spec
# this function just for pedro.
    def parse_cs(self,contents, label_name, branch_name):
        for i in range(0, len(contents)):
            if(contents[i].find("@@@@@@@@") != -1 or contents[i].find("&&&&&&&&") != -1):
                contents[i] = contents[i].replace("@@@@@@@@", branch_name);
                contents[i] = contents[i].replace("&&&&&&&&", label_name);
        return contents;

###################################
######## Added by EDUOWAN #########
###################################

##check_in_files() is used for listing all checked out files and checking in them
##success return 0, error return -1
    def check_in_files(self):
        status,output = commands.getstatusoutput('/usr/atria/bin/cleartool lsco -r -s -cview');
        if output.find("Error") > 0:
            print "wrong in ct lsco!!:\n",output
            return -1;
        elif output == '':
            print "NO checkout files be found!!"
            return -1;
        else:
            files = output.split("\n");
            print "checked-out files are:",files

            #check in all files
            comments = raw_input('Now, check in all files,\nplease enter your comments:')
            for CiFile in files:
                cires=commands.getoutput('/usr/atria/bin/cleartool ci -c \"'+comments+'" '+CiFile)
                if cires.find("Error") != -1 :
                    print "An error happens!!!"
                    print cires;
                    return -1;
                print cires
            return 0;

## getbrname() is used for printing and returning branch name
#  if branch name cannot be found, it also can be input mannually
    def getbrname(self):
        cs = commands.getstatusoutput('/usr/atria/bin/cleartool catcs')
        cs= str(cs[1])
        cs = cs.split(',')
        brname = cs[0].split()
        if "-mkbranch" in brname:
            brno = brname.index("-mkbranch")
            branch_name = brname[brno+1]
            print 'your branch name is',branch_name
        else:
            run = True;
            while (run):
                print 'can not find branch name predefined! Manually input now';
                branch_name = raw_input('please enter the branch name:');
                if branch_name == '':
                    print 'branch name can not be null!!';
                else:
                    print 'your branch name is',branch_name;
                    run = False;
        return branch_name


##create_label() is used to create a labeltype
    def create_label(self):
        try:
            print 'creating label...';
            run = True;
            while (run):
                _LabelName = raw_input('\nNow create a labeltype(capital letters):');
                LabelName = _LabelName.upper();#in case input lower-case letters
                if LabelName.upper() == '':
                    print "NULL label name !!! try again!"
                else:
                    run = False;
            createlabel = commands.getoutput('/usr/atria/bin/cleartool mklbtype -nc '+LabelName);
            print createlabel;
            print "\nCreated label:",LabelName;
        except Exception:
            print '\nSth. wrong in create_label()! Pls check!\n';
        return LabelName

## used to choose the latest 10 labels, used for find "PEDRO_TEAM" or USER defined keyword
    def choose_label(self,keyword = "PEDRO_TEAM"):
        try:
            print "DEFAULT choose label like I##_PEDRO_TEAM_XXXX_XXXX, only this pattern can be found and sort correctly, used for PEDRO, ## is SP No.\n\n";#this can be improved
            label_list = commands.getoutput('/usr/atria/bin/cleartool lstype -kind lbtype -short | grep '+keyword+' | sort -r -n -t I -k 2');
            label_list = label_list.split('\n')
            if label_list[0] == '':
                print "NONE LABEL FOUND!"
                return;
            elif len(label_list) > 10 :
                MAX = 10;
            else:
                MAX = len(label_list);
            print 'Total %d labels. Latest %d labels are:' % (len(label_list), MAX);
            for elem in range(0,MAX):
                print elem,':',label_list[elem];
            run = True;
            while (run):
                label_choose = raw_input('Please choose a label:')
                if label_choose.isdigit() == False :
                    print "please input a No.!"
                elif int(label_choose) > 9 :
                    print "Wrong chosen!"
                else:
                    label_choose = int(label_choose);
                    print "choosing",label_choose,':',
                    label = label_list[label_choose].split(' ');# remove "(lock)" in label
                    print label[0];
                    run = False;
            return label[0];
        except :
            print '\nSth. wrong in choose_label()! Pls check!\n';



##mk_label() is used to apply label on LATEST files in branch
    def mk_label(self,LabelName,brname):
        try:
            if LabelName == '' or brname == '' :
                print "mk_label() should be given args of LabelName and brname!";
                return -1;
            labelfiles = self.sd.runCmd2('cleartool find . -version \'version(.../'+brname+'/LATEST)\' -print -nxname');
            print labelfiles
            for eachfile in labelfiles:
                print eachfile;
                os.system('/usr/atria/bin/cleartool mklabel '+LabelName+' '+eachfile);
                #os.system('/usr/atria/bin/cleartool mklabel '+LabelName+' '+labelfiles);
        except Exception:
                print '\nSth. wrong in mk_label()! Pls check!\n';
                sys.exit();
        return 0;


##  used to lock the specified label
    def lock_label(self,LabelName):
        try:
            print "locking label...."
            os.system('/usr/atria/bin/cleartool lock -nuser ngrbscnsh lbtype:'+LabelName)
            os.system('/usr/atria/bin/cleartool lstype -l lbtype:'+LabelName)
        except Exception:
            print '\nSth. wrong in lock_label()! Pls check!\n';
            sys.exit();
        return 0;







if __name__ == "__main__":
    p = Vob();
#    list = p.get_label("SP", "PEDRO_TEAM", "/vobs/appsim2");
#    print list[0];
    p.generate_cs("config_spec", "",">>>>>>>>",">>>>>>>>");
