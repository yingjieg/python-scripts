#!/usr/bin/env python
#coding=utf-8

import os
import sys
from xml.dom import minidom

profile = "profile.xml";

class Pedro:
    CP_list = [];
    def __init__(self, board_type):
        self.revisions = {};
        blockElement = self.getBlocks_xml(board_type); # parse the xml file
        self.makefiles = self.getMakefile_xml(blockElement);
        self.pedro_list = self.getBlockList_xml(blockElement);
        self.program_defs = self.getProgramdef_xml(blockElement);

    def print_list(self):
        for key in Pedro.program_defs:
            print Pedro.program_defs[key];

    #get the R state from program.def file
    def get_revision(self):
        for key in self.program_defs:
            if self.check_file(self.program_defs[key]) == False:
                print "Can not find program.defs, please check";
                return;
            fd = open(self.program_defs[key],'r');
            try:
                for line in fd:
                    index = line.find("revision", 0,8);
                    if index != -1:
                        self.revisions[key] = line;
                        self.revisions[key] = self.parse_revision(self.revisions[key]);
            finally:
                fd.close();

    # parse the revision line, return the R state
    def parse_revision(self, str):
        if str == None:
            print "error, please check";
            return;
        s = str.split(' '); # split revision and RXXX
        return s[1].strip(); # del the '\n'


    # check the file 
    def check_file(self,file_path):
        result = os.path.isfile(file_path);
        return result;

    def get_pedro_list(self):
        return self.pedro_list;

    def get_makefiles(self):
        return self.makefiles;
                
    def get_program_defs(self):
        return self.program_defs;

    def getBlocks_xml(self, tagName):
        path = os.path.dirname(sys.argv[0]) + '/';
        dom = minidom.parse(path + profile);
        nodes = dom.getElementsByTagName(tagName)[0]; # get element EPB1
        return nodes;

    def getMakefile_xml(self, blockNode):
        makefile_dict = {};
        for node in blockNode.childNodes:
            if node.nodeType == node.ELEMENT_NODE:
                makefile = node.getElementsByTagName("makefile")[0]; # get element makefile
                makefile_dict[str(node.nodeName)] = str(makefile.childNodes[0].data);

        return makefile_dict;

    def getProgramdef_xml(self, blockNode):
        programdef_dict = {};
        for node in blockNode.childNodes:
            if node.nodeType == node.ELEMENT_NODE:
                prodefs = node.getElementsByTagName("prodef")[0]; # get element prodef
                programdef_dict[str(node.nodeName)] = str(prodefs.childNodes[0].data);

        return programdef_dict;

    def getBlockList_xml(self, blockNode):
        blockList = [];
        for node in blockNode.childNodes:
            if node.nodeType == node.ELEMENT_NODE:
                blockList.append(str(node.nodeName));

        return blockList;


if __name__ == "__main__":
    print "hello world";
    p = Pedro("CETB1");
  #  p.get_revision();
#    print p.get_pedro_list();
    print p.get_makefiles();
    print p.get_pedro_list();
    print p.get_program_defs();
#    print p.get_program_defs();
