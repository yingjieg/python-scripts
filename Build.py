#!/usr/bin/env python
#coding:utf-8

import os
import sys
import Pedro
import shell.Shell as shell
import getpass
import shutil

class Build(Pedro.Pedro):
    def __init__(self, board_type):
        PEDRO = Pedro.Pedro(board_type);
        self.makefiles = PEDRO.get_makefiles();
        self.pedro = PEDRO.get_pedro_list();
        self.sd = shell.Shell();
        

    def compile(self):
        for key in self.pedro:
            self.sd.delay(30);
            print "------ compiling " + key + "------";
            os.chdir(self.makefiles[key]);
            status = self.sd.runCmd("clearmake");
            if status != 0:
                print "build " +key+" error, please check";
                sys.exit(1);
#            self.generate_ose(key);
            
    def clean(self):
        for key in self.makefiles:
            self.sd.delay(10);
            os.chdir(self.makefiles[key]);
            self.sd.runCmd("clearmake clean");
        
            
    def cp_file(self, paths):
        for p in self.pedro:
            os.chdir(self.makefiles[p]);
            self.sd.runCmd("cp *.chk EMRP-* *.hex1.elf "+paths[p]);

    def combine_comp(self, path):
        self.sd.runCmd("touch " + path + "/EMRP-COMP");
        out_file = open(path + "/EMRP-COMP", 'w+');
        print "Now combine COMPs";

        for p in self.pedro:
            try:
                in_file = open(self.makefiles[p] + "/EMRP-COMP", 'r');
                out_file.write(in_file.read());
                in_file.close();
            except IOError:
                print self.makefiles[p] + "/EMRP-COMP";
                print "combine error, please check";
        out_file.close();
    
    def generate_ose(self, ose_name):
# here should check the status, need improve.
        self.sd.runCmd("rpgmap -l *.chk");
        status = self.sd.runCmd("msbpe *.hex "+ose_name+".ose");
        if status == 0:
            print "Generate "+ose_name+".ose file successful";
        else:
            print "Can not generate .ose file, check your environment for Toolbox";
    
# zip file
    def zip_file(self, paths):
        for p in self.pedro:
          #  os.chdir(os.path.dirname(paths[p])); #get parent path
            os.chdir(paths[p]);
            self.sd.runCmd("zip -m " + os.path.basename(paths[p]) + ".zip  *");




# create folder directory for pedro's deliver
    def create_dir(self,path_, board_type):
        if path_ == "": #no input , use the default path
            path_ = "/home/"+getpass.getuser()+"/pedro_build";
            if(os.path.exists(path_) == False):
                os.mkdir(path_);

        if(os.path.exists(path_) == False):
            print "Wrong Path, Please check";
            sys.exit(1); 

        if(os.path.exists(path_+"/"+board_type) == True):
            shutil.rmtree(path_+"/"+board_type); #delete exisiting folder

        os.mkdir(path_+"/"+board_type+"");
        os.mkdir(path_+"/"+board_type+"/RP");

        tp = Pedro.Pedro(board_type);
        tp.get_revision();
        revisions = tp.revisions;
        path_list = {};
        for key in self.pedro:
            os.mkdir(path_+"/"+board_type+"/RP/"+key);
            os.mkdir(path_+"/"+board_type+"/RP/"+key+"/"+revisions[key]);
            path_list[key] = path_+"/"+board_type+"/RP/"+key+"/"+revisions[key];
        return path_list;

    
