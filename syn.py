#!/usr/bin/env python
#coding:utf-8

import os
import dircache
import shutil

src = "/proj/axecp_test/CP12/TOOLS/PEDRO";
dest = "/vobs/appsim2/CNL_PEDRO/delivery";

pedro_src = {};
pedro_dest = {};
revision_list = [];
sub_dir = False;

def list_dir(path, pedro_dict):
    global sub_dir;
    dirs = dircache.listdir(path);
    if(sub_dir):
        pedro_dict[path] = dirs;
        
    for d in dirs:
        if os.path.isdir(path+"/"+d) == True:
            if "PEDRO" in d:
                sub_dir = True;
            else:
                sub_dir = False;
            list_dir(path+"/"+d, pedro_dict);
            
def main():
    list_dir(src, pedro_src);
    list_dir(dest, pedro_dest);

    for key in pedro_src.keys():
        for l in pedro_src[key]:
            if (l in pedro_dest[dest+key[len(src):]]) == False:
                try:
                    print "create directory " + dest+key[len(src):]+"/"+l;
                    os.mkdir(dest+key[len(src):]+"/"+l);
                except  OSError, why:
                    print "Faild: %s" % str(why);

                files = dircache.listdir(key+"/"+l);
                for f in files:
                    print "copy "+f+" to" + dest+key[len(src):]+"/"+l;
                    shutil.copy(key+"/"+l+"/"+f, dest+key[len(src):]+"/"+l);

if __name__ == "__main__":
    main();

 
