#!/usr/bin/env python
#coding:utf-8

import sys
import os
import commands
from Vob import *
import shell.Shell as shell
import getpass

'''This script is used to deliver work.
   5 steps of delivering are involved.
   if sd.operation_confirm("xxxxxxx?[yes]") == True: condition control used to skip some operation.
'''

try:
    p=Vob();
    sd = shell.Shell();

    #get branch name
    brname = p.getbrname();
    print "Note: this script should run in intergrate view";

    #rebase
    if sd.operation_confirm("Need to rebase?[yes]") == True:
        rebase_Label = raw_input("Please input Label Name for rebase source:")
        task_brname = raw_input("Please input your task branch name for rebase:")
        if sd.operation_confirm("Rebase Label:"+rebase_Label+"\nTask branchname:"+task_brname+"\nIs that right?[yes]") == True:
            os.system('cleartool findmerge . -fver '+rebase_Label+' -element \'brtype('+task_brname+')\' -c "Rebase from label '+rebase_Label+'" -merge -gmerge -q');

    #merge
    if sd.operation_confirm("merge files?[yes]") == True:
        print "step#1 merge";
        run = True;
        while (run):
            LabelName = raw_input("Please input Label Name of your task for merge:");
            if LabelName == '':
                print "Label name CANNOT be NULL!";
            else:
                run = False;
                user = getpass.getuser();
                os.system('cleartool findmerge . -fver '+LabelName+' -c "Delivery by '+user+' from label '+LabelName+'\" -merge -gmerge');
            print "step#1 finished"

    #check in
    if sd.operation_confirm("check in files?[yes]") == True:
        print "step#2 check in merged files"
        if p.check_in_files() != 0:
            print "check_in_files() error! pls check!!"
            sys.exit();
        print "step#2 finished."

    if sd.operation_confirm("create/choose a label?[yes]") == True:
        #create label for merged files
        print "step#3 creating label"
        choose = raw_input('Do you wanna choose the latest 10 labels? [yes]')
        if choose in ('','yes','ye','y'):
            LabelName2 = p.choose_label();
        else:
            LabelName2 = p.create_label();
        print "step#3 finished."

        #mklabel
    if sd.operation_confirm("apply label?[yes]") == True:
        print "step#9 applying label"
        os.chdir("/vobs/appsim2");
        os.system('cleartool mklabel -rec '+LabelName2+' .')
        print "step#9 finished."

    #locking label
    if sd.operation_confirm("locking label?[yes]") == True:
        print "step#5 locking label"
        sd.runCmd2('cleartool lock -nuser ngrbscnsh lbtype:'+LabelName2);
        print "step#5 finished.";

except RuntimeError:
    print 'function error!';
    sys.exit();
