#!/bin/tcsh

set envdir=`dirname $1`

cd $envdir 
pwd
echo "source $1"
source $1

cd $2
pwd
clearmake clean
clearmake all
